# -*- coding: UTF-8 -*-

import requests
import re
import os.path
import time
import json


class VK:

    def __init__(self):
        self.email = 'vkontakte-user@mail.ru'  # your VK user email
        self.password = 'my-very-secret-password'  # your VK user password
        self.vkid = '12345678'  # your VK user id
        self.songs_per_page = 10
        self.headers = { 
            'User-Agent': 'Mozilla/5.0 (Windows; U; Windows NT 6.0; ru; '+\
                          'rv:1.9.2.13) Gecko/20101203 Firefox/3.6.13 ( '+\
                          '.NET CLR 3.5.30729)'
        }

################################################################################
# SEARCH METHODS                                                               #
################################################################################

    def search(self, query, offset=0):

        ''' @type query str
            @type offset int
            @return list

            Preforms a VKontakte music search by given query with given offset.
        '''

        songs = list()
        cookie = self.get_cookie()
        offset = abs(offset)

        posts = {
            'act': 'search', 'al': '1', 'gid': '0',
            'id': self.vkid, 'offset': offset, 'q': query,
            'sort': '2' , 'count': '10'
        }

        response = requests.post(
                        'http://vkontakte.ru/audio',
                        cookies={'remixsid': cookie},
                        headers= self.headers,
                        data = posts)
        response = response.content.encode('utf-8')
        songs = self.parse_songs(response)

        if songs:
            return songs
        return []

    def search_by_id(self, id):

        ''' @type id str
            @return dict
            
            Performs search of unique songs in vkontakte.
        '''

        if self.is_valid_id(id):
            cookie = self.get_cookie()
            user_id, song_id = id.split('_')

            posts = {
                'act': 'load_audios_silent', 'edit': '0',
                'gid': '0', 'id': user_id, 'al': '1'
            }
            response = requests.post(
                            'http://vkontakte.ru/audio',
                            cookies={'remixsid': cookie},
                            headers=self.headers, data=posts
                        ).content

            response = response.split('<!>')[5].split(':', 1)[1]
            response = response.replace('"', '&quote;')
            response = response.replace('\',\'', '","')
            response = response.replace('[\'', '["')
            response = response.replace('\']', '"]')
            response = json.loads('{"all":' + response.encode('utf-8'))

            for song in response['all']:
                if song[1] == song_id:
                    response = song
            
            return  {'id': id, 'duration': song[4], 'url': song[2],
                    'title': song[6],'artist': song[5]}
        return {}

    def parse_songs(self, unparsed_songs):

        ''' @type hash str
            @return list
            
            Parses search results
        '''

        songs = list()
        split_line = '<div class="fl_l" style="width:31px;height:21px;">'
        unparsed_songs = unparsed_songs.split(split_line)

        if unparsed_songs:
            for idx, song in enumerate(all_songs):
                try:  # some song cannot be parsed, so we need to avoid crash
                    # getting inner tags with song info
                    regex = r'<div class="title_wrap">(.*)<\/div>'
                    song_title = re.search(regex, song, flags=re.I).group(0)
                    # removes song holder name and tags
                    regex = r'<small>(.*)</small>'
                    song_title = re.sub(regex, '', song_title)
                    # removes all other tags from song name
                    regex = r'<[^>]*?>'
                    song_title = re.sub(regex, '' , song_title)
                    # getting song title and artist
                    song_artist, song_name = song_title.split(' - ', 1)
                    # getting song duration
                    regex = r'<div class="duration fl_r".+?>(.*)<\/div>'
                    song_dur = re.search(regex, song, flags=re.I).group(1)
                    # getting raw id and link
                    regex = r'<input type="hidden" id="(.*)" value="(.*)" />'
                    song_link_n_id = re.search(regex, song, flags=re.I).groups()
                    song_id, song_link = song_link_n_id
                    # getting id
                    song_id = song_id.replace('audio_info', '')
                    # getting link
                    song_link = song_link.split(',')[0]
                    songs.append({
                        'id': song_id,
                        'duration': song_dur,
                        'url': base64.b64encode( song_link ),
                        'title': song_name,
                        'artist': song_artist
                    })
                except:
                    pass
                # breaking parsing if songs per page limit reached
                if idx >= self.songs_per_page - 1:
                    break
            if songs:
                return songs
        else:
            self.login()  # reset vk cookie
        return []

################################################################################
# VKONTAKTE AUTENTICATION METHODS                                              #
################################################################################

    def get_cookie(self):

        ''' @return str

            Returns vkontakte cookie from file
        '''

        cookie = ''
        if os.path.exists('cookies/vkcookie'):
            file = open('cookies/vkcookie', 'r')
            cookie = file.read()
            file.close()
        if not cookie:
            cookie = self.login()
        return cookie

    def login(self):

        ''' @return str

            Performs relogin on vkontakte and cookie renewal
        '''

        cookie = self.auth()
        if cookie:
            file = open('cookies/vkcookie', 'w')
            file.write(cookie)
            file.close()
            return cookie
        return ''

    def auth(self):

        ''' @return str

            Gets new cookie from vkontakte
        '''

        link = 'http://vk.com/login.php?op=a_login_attempt'
        # first line authentication
        if requests.post(link).content == 'vklogin':
            posts = {'act': 'login', 'success_url': '', 'fail_url': '',
                    'try_to_login': '1', 'to': '', 'vk': '1', 'al_test': '3',
                    'email': self.email, 'pass': self.password, 'expire': ''}
            # posting initial info
            link = 'http://login.vk.com/'
            response = requests.post(link, headers=self.headers, data=posts)
            # trying to reach auth hash
            regex = r'([0-9a-f]){25,35}'
            hash = re.search(regex, response.headers['location']).group(0)
            # trying to reach user cookie
            link = 'http://vk.com/login.php?act=slogin&fast=1&hash=%s&s=1'%hash
            regex = r'([0-9a-f]){55,65}'
            response = requests.post(link, headers=self.headers)
            cookie = re.search(regex, response.headers['set-cookie'] ).group(0)
            return cookie
        return ''

    def is_valid_id(self, id):

        ''' @type id str
            @return bool
        
            Checks if artist or track has russian characters.
        '''

        id = id.replace('_', '.')
        try:
            float(id)
            return True
        except:
            return False
