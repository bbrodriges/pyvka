### About

This python module is a set of unofficial hooks for vk.com (vkontakte.ru) to search and listen audios from this site. Strange name is based on **Py**thon **Vk**ontakte **A**udio.

### Requirements

* [Requests](http://python-requests.org/)

### Usage:

Edit configuration lines in VK class and import pyvka in your project.

``` python
from pyvka import *

vkontakte = VK()

hundred_pink_floyd_songs = vkontakte.search('Pink Floyd') #contains all songs for "Pink Floyd" query
the_very_best_pink_floyd_song = vkontakte.search_by_id('34607133_704863146') #contains one unique song searched by id
```

### Warning:

Please, keep in mind that this module does not use any official API calls. You can lose your vk.com account. Do everything at your own risk.

### License:

You may do with this code whatever you want.

### Credits:

Partially based on [OpenPlayer project](https://github.com/uavn/openplayer)

